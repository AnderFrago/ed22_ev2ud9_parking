package org.cuatrovientos.ed;

public class Space {
	
	private int id;
	private Car car;
	
	public Space(int id, Car car) {
		super();
		this.id = id;
		this.car = car;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	@Override
	public String toString() {
		return "Space [id=" + id + ", car=" + car + "]";
	}
	

}
