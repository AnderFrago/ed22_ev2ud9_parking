package org.cuatrovientos.ed;

public class Car {
	
	private String plate;

	
	public Car(String plate) {	
		this.plate = plate;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	
	public String toString() {
		return "Car [plate=" + plate + "]";
	}

}
