package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Parking {
	
	private static final int MAX_SIZE = 10;
	
	private ArrayList<Space> spaces;
	/*
	 * La ventaja de usar un ArrayList es que no tenemos l�mite m�ximo
	 * y adem�s, nos ofrece funciones para a�adir y quitar elementos f�cilmente
	 * Car[] cars = new Car[100];
	 */

	public Parking() {
		this.spaces = new ArrayList<Space>();
		initialize();
	}
	
	private void initialize() {
		// Instanciar cada uno de los espacios que van en el Parking
		for (int i = 0; i < MAX_SIZE; i++) {
			// Ahorro el generar la variable sp, que no voy a volver a utilizar.
			// Space sp = new Space(i, null);
			this.spaces.add(new Space(i, null));	
		}		
	}
	
	public boolean add(Car car) {
		if(spaces.get(MAX_SIZE-1).getCar()== null ){
			for (Space space : spaces) {
				if(space.getCar() == null) {
					space.setCar(car);
					break;
				}
			}	
			return true;
		}
		return false;
	}
	
	public boolean remove(String plate) {
		for (Space space : spaces) {
			if(space.getCar() != null)
			{
				// Car c = space.getCar();
				// if(c.getPlate().equals(plate)) {
				if(space.getCar().getPlate().equals(plate)) {
					// Existe un coche con esa matr�cula
					// ...lo quito
					space.setCar(null);
					return true;
				}		
			}
		}
		// No he podido quitar un coche con esa matr�cula (No entra al if)
		return false;
	}
	
	public String showPlate(int number) {
		if((number >= 0) && (number < MAX_SIZE) ) {
			if(this.spaces.get(number).getCar() != null) {
				return this.spaces.get(number).getCar().getPlate();
			}
		}
		return "N�mero incorrecto";
	}
	
	public String showAll() {
		String result = "";
		for (Space space : spaces) {
			if(space.getCar() != null) {
				result += space.getCar().toString();
			}
		}
		return result;
	}	
}
