package org.cuatrovientos.ed;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		String option = "";	
		Parking parking = new Parking();

		Scanner reader = new Scanner(System.in);
		do {
			// Mostrar men�
			menu();
			option = reader.nextLine();
			stateMachine(option, parking);
			
		}while(!option.equals("5"));	

	}
	
	
	private static void menu() {
		System.out.println("[o===o]----Welcome to Parking Manager----[o===o]");
		System.out.println("Select an option:");
		System.out.println("1. Add a new car");
		System.out.println("2. Delete a car");
		System.out.println("3. Show place status");
		System.out.println("4. Show all palces");
		System.out.println("5. Exit");
	}
	
	private static void stateMachine(String option, Parking parking) {
		Scanner reader = new Scanner(System.in);
		String plate = "";
		int number = 0;
		//String option = "";
		
		switch (option) {
		case "1":
			// Add a new car
			System.out.println("Please, enter car plate: ");
			plate = reader.nextLine(); 
			
			Car car = new Car(plate);
			
			if(parking.add(car)) {
				System.out.println("Car"+plate+" added");
			} else {
				System.out.println("Car can not be added");
			}
			break;	
		case "2":
			// Delete a car
			System.out.println("Please, enter car plate to remove");
			plate = reader.nextLine();
			
			if(parking.remove(plate)) {
				System.out.println("Car "+plate+" removed");
			} else {
				System.out.println("Car can not be removed");
			}						
			break;				
		case "3":
			// Show place status
			System.out.println("Please, enter place identifier");
			String tmp = reader.nextLine();
			number = Integer.parseInt(tmp);
			
			System.out.println(parking.showPlate(number));
			break;	
		case "4":
			// Show all places
			System.out.println(parking.showAll());
			break;
		default:
			break;
		}
	}
	
	
	

}
